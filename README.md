# ASCII Art Web Program

## Introduction
This guide provides step-by-step instructions on running the `ascii-art-web` program and viewing the results in your web browser.

## Prerequisites
Ensure that you have [Go](https://golang.org/) installed on your system.

## Running the Program
1. Open your terminal.
2. Navigate to the directory containing the `ascii-art-web` program.

```bash
cd /path/to/ascii-art-web
```

1. Run the program using the following command:

```bash
go run .
```

The program will start, and you should see output indicating that the web server is running.

2. Accessing the Web Page
    Open your web browser.
    Enter the following URL in the address bar:
    http://localhost:8080
    Press Enter.

    You should now see the ASCII Art web page with the generated art.

3. Stopping the Program
    To stop the program, go back to the terminal and press Ctrl + C.

That's it! You've successfully run the ascii-art-web program and viewed the results in your web browser.